import { combineReducers } from "redux";
import courses from "./courseReducer";
import authors from "./authorReducer";
import apiStatusReducer from "./apiStatusReducer";

//Big Note: The properties in combineReducers must match the initial state keys
//I think it is mapping the reducer to the slices of state it updates
const rootReducer = combineReducers({
  courses: courses,
  authors: authors,
  apiCallsInProgress1: apiStatusReducer,
});

export default rootReducer;
