import { actionTypes } from "../actions/actionTypes";
import initialState from "./initialState";

//Thunks dispatch action types that end with _SUCCESS when they complete
//So here we are using this helper method to know when the action ended
function actionTypeEndsInSuccess(type) {
  return type.substring(type.length - 8) === "_SUCCESS";
}

export default function apiStatusReducer(
  state = initialState.apiCallsInProgresss,
  action
) {
  if (action.type == actionTypes.BEGIN_API_CALL) {
    return state + 1;
  } else if (actionTypeEndsInSuccess(action.type)) {
    return state - 1;
  }

  return state;
}
