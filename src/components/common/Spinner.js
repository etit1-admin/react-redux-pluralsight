import React from "react";
import "./Spinner.css";
import Loader from "react-loader-spinner";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

const Spinner = () => {
  //   return <div calssName="loader">Loading ...</div>;
  return <Loader type="Oval" color="#00BFFF" height={80} width={80} />;
};

export default Spinner;
